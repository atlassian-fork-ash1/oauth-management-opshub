const basePathMaintenance = '/api/maintenance';
const basePathClientGrants = '/api/clientGrants';

const oauthManagmentService = service => ({
  validateClientGrants: (clientId, validationRequest) => $.ajax({
          url: service.proxyUrl(`${basePathMaintenance}/clients/${clientId}/validateClientGrants`),
          type: 'POST',
          headers: {
                'Content-Type':'application/json'
          },
          data: JSON.stringify(validationRequest)}),
  manageClientGrants: (installRequest) => $.ajax({
          url: service.proxyUrl(`${basePathClientGrants}`),
          type: 'POST',
          headers: {
                'Content-Type':'application/json'
          },
          data: JSON.stringify(installRequest)})
  });
